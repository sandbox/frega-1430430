var express = require('express');
var app = require('express').createServer();
var path = require('path');

exports.createPresenceImageServer = function(presence, config) {
  var debug = config.debug || false;

  app.configure(function(){
    app.use(app.router);
  });

  app.get('/user/:id', function(req, res, next){
    presence.getPresence('user', parseInt(req.params.id, 10), {}, function(err, data) {
      debug && console.log('/user/', req.params, data);
      if (err || !data || !data.status) {
        res.send(404);
      } else {
        debug && console.log(__dirname + '/img/' + path.basename(data.status) + '.png');
        res.sendfile(__dirname + '/img/' + path.basename(data.status) + '.png');
      }
    });
  });
  app.listen(config.presenceImageServer.port);
  debug && console.log('Started presenceImageServer (express) on port ', config.presenceImageServer.port);
}
