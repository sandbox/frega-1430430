var async = require('async');
var _ = require('underscore');
var util = require('util');
var dnode = require('dnode');

// @todo npm-ify!
var EventedStore = require('../../../dnode_faye/ssjs/eventedstore').EventedStore;

var presence = function(config) {
  this.setConfig(config);
}

presence.prototype.setConfig = function(config) {
  config = config || {};
  config.prefix = config.prefix || '';
  config.status = config.status || [];
  config.inactiveState = config.inactiveState || 'offline';
  // 5 min als inactive timeout
  config.inactiveTimeout = config.inactiveTimeout || 300000;

  config.dataStore = config.dataStore || {'type': 'redis', 'prefix': config.prefix};
  this.dataStore = EventedStore.createStore(config.dataStore);

  this.config = config;
  this.debug = true;

  this.eventEmitter = require('events').EventEmitter;
};


presence.prototype.getPresencesByStatus = function(status, cb) {
  var self = this;
  this.dataStore.smembers('s-' + status, function(err, data) {
    if (err) {
      return cb(err);
    }
    self._retrieveUserDataById(data, cb);
  });
}

presence.prototype.countPresencesByStatus = function(status, cb) {
  this.dataStore.scard('s-' + status, cb);
}


/**
 * Get Presence
 * @param id
 * @param status
 * @param payload
 * @param cb
 */
presence.prototype.getPresence = function(id, cb) {
  this.dataStore.get('u-' + id, cb);
};

presence.prototype.getPresencesByStatusIntersect = function(status, intersect_key, cb) {
  var self = this;
  this.dataStore.sinter('s-' + status, intersect_key, function(err, data) {
    if (err) {
      return cb(err);
    }
    self._retrieveUserDataById(data, cb);
  });
}

presence.prototype._retrieveUserDataById = function(data, cb) {
  var self = this;
  async.map(data, function(id, async_cb) {
    self.getPresence(id, function(err, d) {
      if (err) {
        return async_cb(err);
      }
      d.id = id;
      async_cb(err, d);
    })
  }, cb);
}

/**
 * Set Presence
 * @param id
 * @param status
 * @param payload
 * @param cb
 */
presence.prototype.setPresence = function(id, status, payload, cb) {
  var self = this;
  this.getPresence(id, function(err, data) {
    self.debug && console.log('getPresence - current', data);
    // store the current status
    var current  = (data && data.status) ? data : {};
    self.debug && console.log('status identical?', (current.status == status) , 'payload?', _.isEqual(current.payload, payload), util.inspect(current.payload), util.inspect(payload));
    var ts = + (new Date());
    if ((current.status == status) && (_.isEqual(current.payload, payload))) {
      self.debug && console.log('setPresence - identical status and payload updating atime');
      // track access time
      current.atime = ts;
      self.dataStore.set('u-' + id, current, function(err, data) {
        if (current.status != self.config.inactiveState)  {
          self.touch(id, cb);
        }
        cb(err, data);
      });
      return ;
    }
    // if payload is null, try using existing payload
    if (payload === null && current.payload) {
      payload = current.payload;
    }
    // set presence
    var p = {
      'id': id,
      'status': status,
      'payload': payload,
      'previous': {
        'status': current.status, 'payload': current.payload, 'mtime': current.mtime
      },
      'mtime': ts,
      'atime': ts
    };
    // otherwise set it, track change
    self.dataStore.set('u-' + id, p, function(err, data) {
      if (err) {
        return cb(err);
      }
      self.debug && console.log('self.dataStore.set', 'u-' + id, {'status': status, 'payload': payload, 'previous': current});
      // remove old status
      if (current.status) {
        self.dataStore.srem('s-' + current.status, id, function(err, data) {
          self.debug && console.log('Removed old status', err, current.status);
        });
      }
      // add to new status-set
      self.dataStore.sadd('s-' + status, id, function() {
        // @todo: use EventEmitter instead and do this in dnode_presence.dnode.js
        console.log('broadcastPresenceChanges', self.config.broadcastPresenceChanges, p);
        if (self.config.broadcastPresenceChanges && self.config.broadcastPresenceChanges.enabled) {
          dnode.connect(self.config.broadcastPresenceChanges.fayeDnodePort, function(remote) {
            self.debug && console.log('broadcastPresenceChanges', p);
            remote.publish(self.config.broadcastPresenceChanges.channel, p, function() {
              self.debug && console.log('sent broadcastPresenceChanges', p);
            });
          });
        }
        // and then touch it, if it is not the inactiveState
        if (status != self.config.inactiveState) {
          return self.touch(id, cb);
        }

        cb();
      });
    });
  });
};

/**
 * Touch a user's presence.
 * If he's online, go offline
 * @param id
 * @param cb
 */
presence.prototype.touch = function(id, cb) {
  var self = this;
  this.getPresence(id, function(err, data) {
    if (err) {
      return cb(err);
    }
    var status = data.status;
    if (status != self.config.inactiveState) {
      var ts = +(new Date());
      self.dataStore.zadd('inactivity', ts + self.config.inactiveTimeout, id, cb);
    }
  });
}

/**
 * Run GC to set inactive presences
 */
presence.prototype.inactivityCheck = function(cb) {
  var ts = + (new Date());
  var self = this;
  this.dataStore.zrangebyscore('inactivity', 0, ts, function(err, data) {
    if (err || !data || !data.length) {
      return cb(err, data);
    }
    // we shouldnt get stuck here for too long
    async.forEachLimit(data, 1000, function(id, async_cb) {
      self.setPresence(id, self.config.inactiveState, null, function(err, d) {
        self.dataStore.zrem('inactivity', id, async_cb);
      });
    }, cb);
  });
}

presence.prototype.unsetPresence = function(id, cb) {
  var self = this;
  this.dataStore.unset('u-' + id, function(err, data) {
    if (err) {
      return cb(err);
    }
    async.forEach(self.config.status, function(status, async_cb) {
      self.dataStore.srem('s-' + status, id, function(err, data) {
        self.dataStore.zrem('inactivity', id, async_cb);
      });
    }, cb);
  });
}

exports.presence = presence;
