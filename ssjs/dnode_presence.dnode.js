var redis = require('redis');
var async = require('async');
var presence = require('./lib/presence').presence;

// load config - @todo: proper configuration injection
if (DrupalDnode) {
  var config = DrupalDnode.getModuleConfig('dnode_presence');
} else {
  var config = require('./config.json');
}

var debug = config.debug || false;
// singleton instance map
var pMap = {};

function presenceFactory(category) {
  if (typeof pMap[category]=='undefined') {
    var c = config;
    c.prefix = 'pf-' + category;
    if (c.broadcastPresenceChanges && c.broadcastPresenceChanges.enabled) {
      c.broadcastPresenceChanges.channel = c.broadcastPresenceChanges.baseChannel + '/' + category;
    }
    pMap[category] = new presence(c);
  }
  return pMap[category];
}

var methods = {
  'getPresence': function(category, id, cb) {
    presenceFactory(category).getPresence(id, cb);
  },
  'setPresence': function(category, id, status, payload, cb) {
    debug && console.log('setPresence', category, id, status, payload);
    if (status === false) {
      presenceFactory(category).unsetPresence(id, cb);
    } else {
      presenceFactory(category).setPresence(id, status, payload, cb);
    }
  },
  // "touch" the presence, to extend it's time until it is considered offline
  'touch': function(category, id, cb) {
    presenceFactory(category).touch(id, cb);
  },
  // check if a user is in  a status
  'isStatus': function(category, id, status) {
    presenceFactory(category).getPresence(id, function(err, data) {
      if (err) {
        cb(err);
      } else if (data.status == status) {
        cb(null, true);
      } else {
        cb(null, false);
      }
    });
  },
  // remove "stale" presences
  'inactivityCheck': function(cb) {
    // @todo: store the categories, because currently the inactivityCheck relies on
    // the pMap to run "inactivityCheck", i.e. if no Presence is queried and/or set no inactivity check is run!
    var categories = [];
    for (var category in pMap) {
      categories.push(category);
    }
    async.map(categories, function(category, async_cb) {
      presenceFactory(category).inactivityCheck(async_cb);
    }, cb);
  },
  'getPresencesByStatus': function(category, status, cb) {
    presenceFactory(category).getPresencesByStatus(status, function(err, data) {
      debug && console.log('getPresencesByStatus('+status+')', data);
      cb(err, data);
    });
  },
  'countPresencesByStatus': function(category, status, cb) {
      presenceFactory(category).countPresencesByStatus(status, function(err, data) {
        debug && console.log('countPresencesByStatus('+status+')', data);
        cb(err, data);
      });
    },
  /**
   * Get the Presences
   * @param category
   * @param status
   * @param intersect_key
   * @param cb
   */
  'getPresencesByStatusIntersect': function(category, status, intersect_key, cb) {
    presenceFactory(category).getPresencesByStatusIntersect(status, intersect_key, function(err, data) {
      debug && console.log('getPresencesByStatusIntersect('+status+', ' + intersect_key + ')', data);
      cb(err, data);
    });
  }
}
// export methods
for (var i in methods) {
  exports[i] = methods[i];
}

// default dnodeConfig
exports.config = {
  serverId: 'dnode_presence',
  port: 5057
};

// setup the inactivtiyCheck interval
setInterval(function() {
  exports.inactivityCheck(function(err, data) {
    debug && console.log('dnode_presence.dnode.js: completed inactivity check', err, data);
  });
}, config.inactivityCheckInterval);

// static images
if (config.presenceImageServer.enabled) {
  var createPresenceImageServer = require('./lib/presenceimageserver').createPresenceImageServer;
  createPresenceImageServer(exports, config);
}
