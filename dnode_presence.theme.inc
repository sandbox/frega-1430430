<?php
/**
 * @file
 * Theme functions for dnode_presence.
 */

/**
 * Returns an image tag to a presence status image for the given user account.
 *
 * @param array $variables
 *   Variables passed in.
 *
 * @return string
 *   Image file referencing the presence image server url.
 */
function theme_dnode_presence_user_status_image($variables = array()) {
  $account = $variables['account'];
  $category = $variables['category'];
  // @Todo: check if the presenceImageServer is actually running.
  $base_url = variable_get('dnode_presence_image_server_url', 'http://' . $_SERVER['HTTP_HOST'] . ':9999');
  return theme('image', array(
    'attributes' => array(
      'src' => $base_url . '/user/' . $account_uid,
      'alt' => t('Presence status for user'),
      'title' => t('Presence status for user'),
    ),
  ));
}
