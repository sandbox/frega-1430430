simple presence functionality (online, offline plus payload and automatic
inactivity check), redis-backed.

The online/offline icons are copyright FAMFAMFAM.
http://www.famfamfam.com/lab/icons/silk/
